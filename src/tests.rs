// dvd-ac - access control for video DVDs
// Copyright (C) 2017 Cooper Paul EdenDay
//
// To the extent possible under law, the author(s) have dedicated all copyright and related
// and neighboring rights to this software to the public domain worldwide. This software is
// distributed without any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication along with this software.
// If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>.

use super::*;

extern crate tokio_tls;
extern crate url;

use tokio_core::reactor::Remote;
use tungstenite::handshake::client::Request;
use self::url::Url;
use std::borrow::Cow;

#[test]
fn login_success() {
    start_test(|remote| {
        async_block!({
            let (mut writer, reader) = await!(prepare_stream(remote, String::from("192.168.1.5")))?;

            writer = await!(writer.send(Message::from(String::from("cedenday"))))?;
            await!(writer.send(Message::from(String::from("password"))))?;

            let (status, _) = await!(get_message_text(reader))?;

            assert_eq!(&status, "Authorized");

            Ok(())
        })
    });
}

#[test]
fn login_failure_password() {
    start_test(|remote| {
        async_block!({
            let (mut writer, reader) = await!(prepare_stream(remote, String::from("192.168.1.5")))?;

            writer = await!(writer.send(Message::from(String::from("cedenday"))))?;
            await!(writer.send(Message::from(String::from("wrong_password"))))?;

            let (status, _) = await!(get_message_text(reader))?;

            assert_eq!(&status, "Unauthorized");

            Ok(())
        })
    });
}

#[test]
fn login_failure_username() {
    start_test(|remote| {
        async_block!({
            let (mut writer, reader) = await!(prepare_stream(remote, String::from("192.168.1.5")))?;

            writer = await!(writer.send(Message::from(String::from("cooper"))))?;
            await!(writer.send(Message::from(String::from("password"))))?;

            let (status, _) = await!(get_message_text(reader))?;

            assert_eq!(&status, "Unauthorized");

            Ok(())
        })
    });
}

#[test]
fn login_failure_both() {
    start_test(|remote| {
        async_block!({
            let (mut writer, reader) = await!(prepare_stream(remote, String::from("192.168.1.5")))?;

            writer = await!(writer.send(Message::from(String::from("cooper"))))?;
            await!(writer.send(Message::from(String::from("wrong_password"))))?;

            let (status, _) = await!(get_message_text(reader))?;

            assert_eq!(&status, "Unauthorized");

            Ok(())
        })
    });
}

#[test]
fn list_titles() {
    start_test(|remote| {
        async_block!({
            let (mut writer, mut reader) =
                await!(prepare_stream(remote, String::from("192.168.1.5")))?;

            writer = await!(writer.send(Message::from(String::from("cedenday"))))?;
            await!(writer.send(Message::from(String::from("password"))))?;

            let (status, new_reader) = await!(get_message_text(reader))?;
            reader = new_reader;

            assert_eq!(&status, "Authorized");

            let (raw_titles, _) = await!(get_message_text(reader))?;
            let titles: HashSet<String> = serde_json::from_str(&raw_titles)?;

            assert_eq!(titles.len(), 2);
            assert!(titles.contains("BIG_BUCK_BUNNY"));
            assert!(titles.contains("SINTEL"));

            Ok(())
        })
    });
}

#[test]
fn checkout() {
    start_test(|remote| {
        async_block!({
            let (mut writer, mut reader) =
                await!(prepare_stream(remote, String::from("192.168.1.5")))?;

            writer = await!(writer.send(Message::from(String::from("cedenday"))))?;
            writer = await!(writer.send(Message::from(String::from("password"))))?;

            let (_status, new_reader) = await!(get_message_text(reader))?;
            reader = new_reader;

            let (_raw_titles, new_reader) = await!(get_message_text(reader))?;
            reader = new_reader;

            await!(writer.send(Message::from(String::from("BIG_BUCK_BUNNY"))))?;

            let (status, _) = await!(get_message_text(reader))?;
            assert_eq!(&status, "Ready");

            Ok(())
        })
    });
}

fn start_test<T, F>(function: T)
where
    T: Fn(Remote) -> F,
    F: Future<Item = (), Error = error::Error>,
{
    let mut core = Core::new().unwrap();

    let server = start(core.handle(), Rc::new(PathBuf::from("example_titles"))).unwrap();

    let test = function(core.remote());

    core.run(
        server
            .select(test)
            .map(|(win, _)| win)
            .map_err(|_| Err(()): Result<(), ()>),
    ).unwrap();
}

#[async]
fn prepare_stream(
    remote: Remote,
    real_ip: String,
) -> tungstenite::Result<
    (
        futures::stream::SplitSink<
            tokio_tungstenite::WebSocketStream<
                tokio_tungstenite::stream::Stream<
                    tokio_core::net::TcpStream,
                    tokio_tls::TlsStream<tokio_core::net::TcpStream>,
                >,
            >,
        >,
        futures::stream::SplitStream<
            tokio_tungstenite::WebSocketStream<
                tokio_tungstenite::stream::Stream<
                    tokio_core::net::TcpStream,
                    tokio_tls::TlsStream<tokio_core::net::TcpStream>,
                >,
            >,
        >,
    ),
> {
    let mut request = Request::from(Url::parse("ws://localhost:9998/").unwrap());
    request.add_header(Cow::from("X-Real-IP"), Cow::from(real_ip));

    let websocket = await!(tokio_tungstenite::connect_async(request, remote))?.0;

    Ok(websocket.split())
}
