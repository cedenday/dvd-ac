// dvd-ac - access control for video DVDs
// Copyright (C) 2017 Cooper Paul EdenDay
//
// To the extent possible under law, the author(s) have dedicated all copyright and related
// and neighboring rights to this software to the public domain worldwide. This software is
// distributed without any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication along with this software.
// If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>.

const socket = new WebSocket("ws://" + location.host + "/ws");;

var state = 0;
var dvd_title;

function login() {
    socket.send(document.getElementById("username").value);
    socket.send(document.getElementById("password").value);

    return false;
}

function select() {
    dvd_title = this.innerHTML;
    socket.send(dvd_title);

    return false;
}

function return_item() {
    socket.send("Return");
    socket.close();
    location.reload();
}

socket.addEventListener("message", function (event) {
    switch (state) {
        case 0:
            document.getElementById("header").innerHTML = event.data;
            document.getElementById("login").style.display="none"

            break;
        case 1:
            var titles = JSON.parse(event.data);
            for (title_text of titles) {
                var item = document.createElement("LI");
                var title = document.createElement("A");
                title.href = "#";
                title.onclick = select;

                var title_contents = document.createTextNode(title_text);
                title.appendChild(title_contents);
                item.appendChild(title);

                document.getElementById("title_list").appendChild(item);
                document.getElementById("title_list").style.display = "block";
            }
            break;
        case 2:
            document.getElementById("header").innerHTML = event.data;
            document.getElementById("title_list").style.display = "none";

            if (event.data == "Ready") {
                document.getElementById("return_button").style.display = "inline";

                var player = document.getElementById("player");
                var source = document.createElement("SOURCE");
                source.src = "/files/" + dvd_title;
                source.type = "video/webm";
                player.appendChild(source);
                player.style.display = "block";
            }

            break;
    }
    state++;
});
