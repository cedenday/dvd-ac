// dvd-ac - access control for video DVDs
// Copyright (C) 2017 Cooper Paul EdenDay
//
// To the extent possible under law, the author(s) have dedicated all copyright and related
// and neighboring rights to this software to the public domain worldwide. This software is
// distributed without any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication along with this software.
// If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>.

use tungstenite;
use serde_json;
use hyper;
use std::fmt;
use std::io;
use std::result;

#[derive(Debug)]
pub enum Error {
    Env,
    Websocket(tungstenite::Error),
    Serialization(serde_json::Error),
    Protocol,
    Http(hyper::Error),
}

impl fmt::Display for Error {
    fn fmt(&self, mut formatter: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Error::Env => write!(&mut formatter, "Could not find necessary env var."),
            Error::Websocket(ref error) => write!(&mut formatter, "{}", error),
            Error::Serialization(ref error) => write!(&mut formatter, "{}", error),
            Error::Protocol => write!(&mut formatter, "Protocol violation."),
            Error::Http(ref error) => write!(&mut formatter, "{}", error),
        }
    }
}

impl From<tungstenite::Error> for Error {
    fn from(error: tungstenite::Error) -> Self {
        Error::Websocket(error)
    }
}

impl From<serde_json::Error> for Error {
    fn from(error: serde_json::Error) -> Self {
        Error::Serialization(error)
    }
}

impl<T> From<(tungstenite::Error, T)> for Error {
    fn from(error: (tungstenite::Error, T)) -> Self {
        Self::from(error.0)
    }
}

impl From<io::Error> for Error {
    fn from(error: io::Error) -> Self {
        Error::Websocket(tungstenite::Error::Io(error))
    }
}

impl From<hyper::Error> for Error {
    fn from(error: hyper::Error) -> Self {
        Error::Http(error)
    }
}

pub type Result<T> = result::Result<T, Error>;
