// dvd-ac - access control for video DVDs
// Copyright (C) 2017 Cooper Paul EdenDay
//
// To the extent possible under law, the author(s) have dedicated all copyright and related
// and neighboring rights to this software to the public domain worldwide. This software is
// distributed without any warranty.
//
// You should have received a copy of the CC0 Public Domain Dedication along with this software.
// If not, see <https://creativecommons.org/publicdomain/zero/1.0/legalcode.txt>.

#![feature(ip_constructors, refcell_replace_swap, type_ascription, proc_macro,
           conservative_impl_trait, generators)]

extern crate futures_await as futures;
extern crate hyper;
extern crate serde_json;
extern crate tokio_core;
extern crate tokio_io;
extern crate tokio_tungstenite;
extern crate tungstenite;

use std::process;
use tokio_core::reactor::{Core, Handle};
use std::rc::Rc;
use std::path::PathBuf;
use std::env;
use futures::prelude::*;
use tokio_core::net::{TcpListener, TcpStream};
use std::net::{IpAddr, Ipv6Addr, SocketAddr};
use hyper::server::{Http, Request, Response, Service};
use std::cell::RefCell;
use std::collections::{HashMap, HashSet};
use tokio_tungstenite::accept_hdr_async;
use std::str::FromStr;
use tungstenite::Message;
use std::io;
use futures::stream::SplitStream;
use tokio_tungstenite::WebSocketStream;
use tokio_io::{AsyncRead, AsyncWrite};
use futures::future::FutureResult;
use std::path::Path;
use hyper::{Get, StatusCode};
use hyper::header::{Header, Raw};
use std::fmt;

mod error;
mod ldap;

#[cfg(test)]
mod tests;

fn main() {
    if let Err(error) = real_main() {
        eprintln!("Error: {}", error);

        process::abort();
    }
}

fn real_main() -> error::Result<()> {
    let mut core = Core::new()?;

    let media_dir = Rc::new(PathBuf::from(env::var_os("DVD_DIR")
        .ok_or(error::Error::Env)?));

    let handle = core.handle();
    core.run(start(handle, media_dir)?)?;

    Ok(())
}

fn start(
    handle: Handle,
    media_dir: Rc<PathBuf>,
) -> error::Result<impl Future<Item = (), Error = error::Error>> {
    let listener = TcpListener::bind(&(Ipv6Addr::localhost(), 9998).into(), &handle)?;

    let locks = Rc::new(RefCell::new(HashMap::new()));
    let ips = Rc::new(RefCell::new(HashMap::new()));
    let titles = Rc::new(RefCell::new(HashMap::new()));

    let http_handle = handle.clone();

    let http_service = {
        let ips = Rc::clone(&ips);
        Http::new().serve_addr_handle(
            &(Ipv6Addr::localhost(), 8080).into(),
            &http_handle,
            move || {
                Ok(Srv {
                    ips: Rc::clone(&ips),
                })
            },
        )?
    };

    let ws_server = listener
        .incoming()
        .for_each(move |(stream, socket)| {
            let media_dir = Rc::clone(&media_dir);
            let locks = Rc::clone(&locks);
            let ips = Rc::clone(&ips);
            let titles = Rc::clone(&titles);

            handle.spawn(
                server(
                    stream,
                    locks,
                    Rc::clone(&ips),
                    Rc::clone(&titles),
                    media_dir,
                ).then(move |result| {
                    if let Err(error) = result {
                        eprintln!(
                            "Warn: {}: {}",
                            titles
                                .borrow()
                                .get(&socket)
                                .unwrap_or(&String::from("NONE")),
                            error
                        );
                    }

                    if let Some(title) = titles.borrow_mut().remove(&socket) {
                        ips.borrow_mut().remove(&title);
                    }

                    Ok(())
                }),
            );

            Ok(())
        })
        .map_err(error::Error::from);

    let http_server = http_service
        .for_each(move |connection| {
            http_handle.spawn(
                connection
                    .map(|_| ())
                    .map_err(|error| eprintln!("Warn: {}", error)),
            );

            Ok(())
        })
        .map_err(error::Error::from);

    Ok(ws_server.join(http_server).map(|_| ()))
}

#[async]
fn server(
    stream: TcpStream,
    locks: Rc<RefCell<HashMap<String, String>>>,
    ips: Rc<RefCell<HashMap<String, IpAddr>>>,
    titles: Rc<RefCell<HashMap<SocketAddr, String>>>,
    media_dir: Rc<PathBuf>,
) -> error::Result<()> {
    let socket_addr = stream.peer_addr()?;

    let client_address = Rc::new(RefCell::new(IpAddr::from(Ipv6Addr::unspecified())));
    let (mut writer, mut reader) = {
        let client_address = Rc::clone(&client_address);
        await!(accept_hdr_async(
            stream,
            move |request: &tungstenite::handshake::server::Request| {
                if let Some(real_ip) = request.headers.find_first("X-Real-IP") {
                    client_address
                        .replace(IpAddr::from_str(std::str::from_utf8(real_ip)?).unwrap());
                }

                Ok(None)
            }
        ))?.split()
    };

    let (username, new_reader) = await!(get_message_text(reader))?;
    reader = new_reader;

    let (password, new_reader) = await!(get_message_text(reader))?;
    reader = new_reader;

    let authorized = await!(is_authorized(username, password))?;

    if let Some(username) = authorized {
        writer = await!(writer.send(Message::text("Authorized")))?;

        let mut media_titles = HashSet::new();
        for entry in media_dir.read_dir()? {
            let entry = entry?;

            if entry.file_type()?.is_file() {
                media_titles
                    .insert(entry.file_name().into_string().map_err(|_| {
                        io::Error::new(io::ErrorKind::InvalidData, "data not UTF-8")
                    })?);
            }
        }

        writer = await!(writer.send(Message::text(serde_json::to_string(&media_titles)?)))?;

        let (media_title, new_reader) = await!(get_message_text(reader))?;
        reader = new_reader;

        if lock(&locks, &media_titles, &media_title, &username) {
            if ips.borrow_mut()
                .insert(media_title.clone(), *client_address.borrow())
                .is_none()
            {
                titles.borrow_mut().insert(socket_addr, media_title.clone());
            }
            await!(writer.send(Message::text("Ready")))?;

            #[async]
            for message in reader {
                let message = message.into_text()?;

                // TODO: ping client regularly.

                if &message == "Return" {
                    unlock(&locks, &media_title);
                    break;
                }
            }
        } else {
            await!(writer.send(Message::text("Unavailable")))?;
        }
    } else {
        await!(writer.send(Message::text("Unauthorized")))?;
    }

    Ok(())
}

#[async]
fn get_message_text<T: 'static>(
    stream: SplitStream<WebSocketStream<T>>,
) -> error::Result<(String, SplitStream<WebSocketStream<T>>)>
where
    T: AsyncRead + AsyncWrite,
{
    let (opt_message, new_stream) = await!(stream.into_future())?;
    let message = opt_message.ok_or(error::Error::Protocol)?;

    Ok((message.into_text()?, new_stream))
}

#[async]
fn is_authorized(username: String, password: String) -> io::Result<Option<String>> {
    if (&username == "cedenday" && &password == "password")
        | (&username == "kedenday" && &password == "password")
    {
        return Ok(Some(username));
    }

    Ok(None)
}

fn lock(
    locks: &Rc<RefCell<HashMap<String, String>>>,
    media_titles: &HashSet<String>,
    media_title: &str,
    username: &str,
) -> bool {
    if let Some(owner) = locks.borrow().get(media_title) {
        return username == owner;
    }

    if !media_titles.contains(media_title) {
        return false;
    }

    locks
        .borrow_mut()
        .insert(media_title.to_owned(), username.to_owned());
    true
}

fn unlock(locks: &Rc<RefCell<HashMap<String, String>>>, media_title: &str) {
    locks.borrow_mut().remove(media_title);
}

struct Srv {
    ips: Rc<RefCell<HashMap<String, IpAddr>>>,
}

impl Service for Srv {
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    type Future = FutureResult<Response, hyper::Error>;

    fn call(&self, req: Request) -> Self::Future {
        futures::future::ok(file_auth(&req, &self.ips))
    }
}

fn file_auth(request: &Request, ips: &Rc<RefCell<HashMap<String, IpAddr>>>) -> Response {
    if let (&Get, Some(title)) = (request.method(), Path::new(request.path()).file_name()) {
        let title = title.to_string_lossy();

        if let Some(client_address) = ips.borrow().get(&title[..]) {
            let server_ip = request.headers().get_raw("X-Real-IP");
            let my_ip = Some(Raw::from(format!("{}", client_address)));

            if server_ip == my_ip.as_ref() {
                let mut path = String::from("/protected/");
                path.push_str(&title);

                return Response::new().with_header(XAccelRedirect(path));
            }
        }
    }

    Response::new().with_status(StatusCode::Unauthorized)
}

#[derive(Clone, Debug)]
struct XAccelRedirect(String);

impl Header for XAccelRedirect {
    fn header_name() -> &'static str {
        static NAME: &'static str = "X-Accel-Redirect";
        NAME
    }

    fn parse_header(_raw: &Raw) -> hyper::Result<XAccelRedirect> {
        unimplemented!();
    }

    fn fmt_header(&self, f: &mut hyper::header::Formatter) -> fmt::Result {
        f.fmt_line(self)
    }
}

impl fmt::Display for XAccelRedirect {
    fn fmt(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        fmt::Display::fmt(&self.0, formatter)
    }
}
